%berechnet die RK-Koeffizienten a_ij des s-stufigen Gauß-Kollokationsverfahrens 

function A = RK_koeffizienten (Stufenzahl)

s = Stufenzahl; 

%Nullstellen der auf das Intervall [0,1] transformierten Legendre-Polynome 

c_1 = 1/2;
c_2 = [(1/6)*(3-sqrt(3)), (1/6)*(3+sqrt(3))];
c_3 = [(1/10)*(5-sqrt(15)), 1/2, (1/10)*(5+sqrt(15))];
c_4 = [(1/70)*(35-sqrt(525+70*sqrt(30))), (1/70)*(35-sqrt(525-70*sqrt(30))), (1/70)*(35+sqrt(525-70*sqrt(30))), (1/70)*(35+sqrt(525+70*sqrt(30)))];
c_5 = [(1/42)*(21-sqrt(7*(35+2*sqrt(70)))), (1/42)*(21-sqrt(7*(35-2*sqrt(70)))), 1/2, (1/42)*(21+sqrt(7*(35-2*sqrt(70)))), (1/42)*(21+sqrt(7*(35+2*sqrt(70))))];
c_6 = [0.0337652428984239, 0.1693953067668685, 0.3806904069583945, 0.6193095930416388, 0.8306046932330680, 0.9662347571016057];    

%schreibe die Nullstellen in eine Matrix
Legendre_Nst = zeros(6,6);
Legendre_Nst(1,1)= c_1;
Legendre_Nst(2,1:2)= c_2;
Legendre_Nst(3,1:3)= c_3;
Legendre_Nst(4,1:4)= c_4;
Legendre_Nst(5,1:5)= c_5;
Legendre_Nst(6,1:6)= c_6;

c = Legendre_Nst(s,1:s); %wähle je nach Stufenzahl passenden Vektor c aus

%erstelle Gleichungssysteme um Zeile i der Koeffizientenmatrix zu berechnen (Berechnung mithilfe der vereinfachenden Bedingungen)
%in den Zeilen der Gleichungssystem-Matrix steht c_i^(q-1)
%auf der rechten Seite (c_i^q)/q

	for i=1:s
	   Matrix(s,s) = 0;
	   rechte_Seite(s) = 0;
	 
		for q=1:s
		   Matrix(q,:) = c.^(q-1);
		   rechte_Seite(q) = (c(i)^q)/q;
		end

%Zeile i der Koeffizientenmatrix A ist Lösung des Gleichungssystems
%Zeilenvektor rechte_Seite muss zu Spaltenvektor transformiert werden
%Befehl A\b löst Gleichungssystem Ax = b

	   A(i,1:s) = Matrix\rechte_Seite';
	end

end;
