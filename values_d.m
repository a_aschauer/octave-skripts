%berechnet die Koeffizienten d_i, die zur Brechnung der nächsten Iterierten des Gauß-Kollokationsverfahrens benötigt werden

function d = values_d (fun1, fun2)
%mit fun1 soll die berechnete RK Koeffizientenmatrix übergeben werden, mit fun2 die berechneten Gewichte

%Zuweisung der übergebenen Werte
A = fun1;
b = fun2;

%es wird die Inverse von A benötigt
invA = inv(A);

%bereche Werte d_i
%da b als Spaltenvektor übergeben wird muss er transformiert werden
d = b'*invA;

end;
